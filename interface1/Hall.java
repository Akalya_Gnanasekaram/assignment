package Ap.qus.interface1;

public interface Hall {
	public int getPrice();

	public String getColour();

	public String getDecoration();

	public double getFront_Area(double x, double y);

	public String getFunction_Type();

}
