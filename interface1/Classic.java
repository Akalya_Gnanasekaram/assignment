package Ap.qus.interface1;

public class Classic implements Hall {

	@Override
	public int getPrice() {
		return 50000;
	}

	@Override
	public String getColour() {
		return "Green";
	}

	@Override
	public String getDecoration() {
		return "Simple Decoration";
	}

	@Override
	public double getFront_Area(double x, double y) {
		return x * y;
	}

	@Override
	public String getFunction_Type() {
		return "Birthday Party";
	}

}
