package Ap.p2.vehiclehierarchy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//RentedVehicle
class RentedVehicle {
	private double baseFee;
	public double Cost;

	public RentedVehicle(double bF, double cost) {
		setBaseFee(bF);
		Cost = cost + bF;
	}

	public void setBaseFee(double baseFee) {
		this.baseFee = baseFee;
	}

	public double getCost() {
		return Cost;
	}

	public void printRentedVehicle() {
		System.out.println("Basic Fee :" + baseFee + "\nTotal cost :" + Cost);
	}

}

//FuelVehicle class
class FuelVehicle extends RentedVehicle {
	private double nbKms;
	public double MileageFees;

	public FuelVehicle(double bF, double cost, double nbkms, double MF) { // nbKms = total number of kilometers traveled
		super(bF, cost);
		nbKms = nbkms;
		// find MileageFees
		if (nbKms < 100) {
			MileageFees = 0.2 * nbKms;
		} else if (nbKms <= 100 && nbKms <= 400) {
			MileageFees = 0.3 * nbKms;
		} else if (nbKms > 400) {
			MileageFees = 0.5 * nbKms;
		}

	}

	public void printFuelVehicle() {
		System.out.println("total number of kilometers traveled :" + nbKms + "Km");
		System.out.println("total MileageFee :" + MileageFees);
	}
}

//Car class

class Car extends FuelVehicle {
	private int nbSeats;
	private String Name;

	public Car(double bF, double cost, double nbkms, double MF, int nbseats, String name) {
		super(bF, cost, nbkms, MF);
		Name = name;
		nbSeats = nbseats;
		Cost = nbseats * bF;
	}

	@Override
	public double getCost() {
		return super.getCost();
	}

	public void printCar() {
		System.out.println("Car name :" + Name + "\nCar cost : " + Cost);
		System.out.println("No of seats : " + nbSeats + "seats");
	}

}

//Truck class
class Truck extends FuelVehicle {
	private int capacity;
	private String Name;

	public Truck(double bF, double cost, double nbkms, double MF, int cpy, String name) {
		super(bF, cost, nbkms, MF);
		capacity = cpy;
		Name = name;
		Cost = bF * cpy;
	}

	@Override
	public double getCost() {
		return super.getCost();
	}

	public void printTruck() {
		System.out.println("Track name :" + Name + "\nTruck cost :" + Cost);
		System.out.println("Truck capacity : " + capacity + "Ton");
	}
}

//Bicycle class
class Bycicle extends RentedVehicle {
	private int nbDays; // number of day it is rented
	private String Name;

	public Bycicle(double bF, double cost, int nbdays, String name) {
		super(bF, cost);
		Name = name;
		nbDays = nbdays;
		Cost = bF * nbdays;
	}

	@Override
	public double getCost() {
		return super.getCost();
	}

	public void printBycicle() {
		System.out.println("Bycicle name : " + Name + "\nBycicle cost : " + Cost);
		System.out.println("No of rented days : " + nbDays + " days");
	}

}

public class VehicleHierarchy {

	public static void main(String[] args) throws IOException {

		int vehicle;
		System.out.print("========= Object ============ \n");
		System.out.print("1. Car     \n");
		System.out.print("2. Truck   \n");
		System.out.print("3. Bicycle \n");
		System.out.print("Enter the object: ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		vehicle = Integer.parseInt(br.readLine());

		switch (vehicle) {
		case 1: {
			Car car = new Car(100, 0, 7, 0, 4, "BMW");
			car.printCar();
		}
			break;
		case 2: {
			Truck tk = new Truck(100, 0, 7, 0, 10, "Betty");
			tk.printTruck();
		}
			break;
		case 3: {
			Bycicle be = new Bycicle(100, 0, 5, "Lumala");
			be.printBycicle();
		}
			break;
		default: {
			System.out.println("Other than these no objects available");
		}
			return;
		}// end 

	}
}

/*
 * 
 * 
 * if(vehicle="fuelehicle" != null) { FuelVehicle fv = new FuelVehicle(100, 0,
 * 700, 0); fv.printFuelVehicle();
 * 
 * }
 */
