package Ap.qus.abstraction;

public class AbstractDemo {

	public static void main(String[] args) {

		Person student = new Employee("Akal", "Female", 18);
		Person employee = new Employee("Thaanu", "Female", 200);

		student.work();
		employee.work();
		employee.changeName("Thaanush");
		student.changeName("Akalya");

		System.out.println(employee.toString());

	}

}
