package Ap.p4.qus.behavioralpattern;

public interface Strategy {

	public float Computation(float x, float y);

}
