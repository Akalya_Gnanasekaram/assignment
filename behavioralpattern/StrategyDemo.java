package Ap.p4.qus.behavioralpattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StrategyDemo {

	public static void main(String[] args) throws NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the num1: ");
		float valuenum1 = Float.parseFloat(br.readLine());
		
		System.out.print("Enter the num2: ");
		float valuenum2 = Float.parseFloat(br.readLine());

		Build bld = new Build(new Sum());
		System.out.println("Sum = " + bld.executeStrategy(valuenum1, valuenum2));

		bld = new Build(new Deduction());
		System.out.println("Deduction = " + bld.executeStrategy(valuenum1, valuenum2));

		bld = new Build(new Multiple());
		System.out.println("Multiplication = " + bld.executeStrategy(valuenum1, valuenum2));
	}

}
