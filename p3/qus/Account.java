package Ap.p3.qus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Account {

		private static Account onlinedb;

		private Account() {

		}

		public static Account getInstance() {
			if (onlinedb == null) {
				onlinedb = new Account();
			}
			return onlinedb;
		}

		private Connection getConnection() throws ClassNotFoundException, SQLException {
			Connection dbconnect = null;
			Class.forName("com.mysql.cj.jdbc.Driver");
			dbconnect = DriverManager.getConnection("JDBC:mysql://localhost:3306/online_shopping", "root", "root");
			return dbconnect;
		}

		// create account data from database
		public void insertData(String id,String name, String billing_address, String open, String close) throws SQLException {
			Connection dbconnect = null;
			PreparedStatement dbstatement = null;
			try {
				dbconnect = this.getConnection();

				dbstatement = dbconnect.prepareStatement("INSERT INTO account (id,name,billing_address,open,close) VALUES(?,?,?,?,?)");
				dbstatement.setString(1, id);
				dbstatement.setString(2, name);
				dbstatement.setString(3, billing_address);
				dbstatement.setString(4, open);
				dbstatement.setString(5, close);
				dbstatement.executeUpdate();
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} finally {
				if (dbconnect != null) {
					dbconnect.close();
				}
				if (dbstatement != null) {
					dbstatement.close();
				}
			}

		}
		// read account data from database
		public void readData(String searchText) throws SQLException {
			Connection dbconnect = null;
			PreparedStatement dbstatement = null;
			ResultSet resultSet = null;
			
			try {
				dbconnect = this.getConnection();
				dbstatement = dbconnect.prepareStatement("SELECT * FROM account WHERE id=?");
				dbstatement.setString(1, searchText);
				resultSet = dbstatement.executeQuery();
				System.out.println("ID\tName");
				while (resultSet.next()) {
					System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2));
				}
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} finally {
				if (resultSet != null)
					resultSet.close();
				if (dbconnect != null) {
					dbconnect.close();
				}
				if (dbstatement != null) {
					dbstatement.close();
				}
			}
		}
		//delete account data from database
		public void deleteData(String id) throws SQLException {			
			Connection dbconnect = null;			
			PreparedStatement dbstatement = null;					
			try {				
				dbconnect = this.getConnection();				
				dbstatement = dbconnect.prepareStatement("DELETE FROM account WHERE id=?");			
				dbstatement.setString(1, id);				
				dbstatement.executeUpdate();			
				} catch (ClassNotFoundException | SQLException e) {				
					e.printStackTrace();			
					} finally {				
						if (dbconnect != null ) {				
							dbconnect.close();			
							}			
						if (dbstatement != null ) {			
							dbstatement.close();		
							}		
						}		
			}
			
class ShoppingCard extends Account{

	@Override
	public void readData(String searchText) throws SQLException {
		// TODO Auto-generated method stub
		super.readData(searchText);
	}
	
	
}
}
