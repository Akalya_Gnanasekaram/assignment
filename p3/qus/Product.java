package Ap.p3.qus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Product {

	private static Product onlinedb;

	private Product() {

	}

	public static Product getInstance() {
		if (onlinedb == null) {
			onlinedb = new Product();
		}
		return onlinedb;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection dbconnect = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		dbconnect = DriverManager.getConnection("JDBC:mysql://localhost:3306/online_shopping", "root", "root");
		return dbconnect;
	}

	// create product items data from database
	public void insertData(String id,String name, String supplier) throws SQLException {
		Connection dbconnect = null;
		PreparedStatement dbstatement = null;
		try {
			dbconnect = this.getConnection();

			dbstatement = dbconnect.prepareStatement("INSERT INTO product (id,name,supplier) VALUES(?,?,?)");
			dbstatement.setString(1, id);
			dbstatement.setString(2, name);
			dbstatement.setString(3, supplier);	
			dbstatement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbconnect != null) {
				dbconnect.close();
			}
			if (dbstatement != null) {
				dbstatement.close();
			}
		}

	}
	// read product details from database
			public void readData(String searctext) throws SQLException {
				Connection dbconnect = null;
				PreparedStatement dbstatement = null;
				ResultSet resultSet = null;
				
				try {
					dbconnect = this.getConnection();
					dbstatement = dbconnect.prepareStatement("SELECT * FROM product WHERE id=?");
					dbstatement.setString(1, searctext);
					//dbstatement.setString(2, name);
					//dbstatement.setString(3, supplier);
					resultSet = dbstatement.executeQuery();
					System.out.println("ID\tName\tSupplier");
					while (resultSet.next()) {
						System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2)+"\t" + resultSet.getString(3));
					}
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				} finally {
					if (resultSet != null)
						resultSet.close();
					if (dbconnect != null) {
						dbconnect.close();
					}
					if (dbstatement != null) {
						dbstatement.close();
					}
				}
			}
}