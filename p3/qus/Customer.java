package Ap.p3.qus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Customer {
	private static Customer onlinedb;

	private Customer() {

	}

	public static Customer getInstance() {
		if (onlinedb == null) {
			onlinedb = new Customer();
		}
		return onlinedb;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection dbconnect = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		dbconnect = DriverManager.getConnection("JDBC:mysql://localhost:3306/online_shopping", "root", "root");
		return dbconnect;
	}

	// create customer data from database
	public void insertData(String id,String password, String address, int phone, String email) throws SQLException {
		Connection dbconnect = null;
		PreparedStatement dbstatement = null;
		try {
			dbconnect = this.getConnection();

			dbstatement = dbconnect.prepareStatement("INSERT INTO customer (id,password,address,phone,email) VALUES(?,?,?,?,?)");
			dbstatement.setString(1, id);
			dbstatement.setString(2, password);
			dbstatement.setString(3, address);
			dbstatement.setInt(4, phone);
			dbstatement.setString(5, email);
			dbstatement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbconnect != null) {
				dbconnect.close();
			}
			if (dbstatement != null) {
				dbstatement.close();
			}
		}

	}
	// read customer data from database
	public void readData(String searchText) throws SQLException {
		Connection dbconnect = null;
		PreparedStatement dbstatement = null;
		ResultSet resultSet = null;
		
		try {
			dbconnect = this.getConnection();
			dbstatement = dbconnect.prepareStatement("SELECT * FROM customer WHERE id=?");
			dbstatement.setString(1, searchText);
			resultSet = dbstatement.executeQuery();
			System.out.println("ID\tPassword\tAddress\tPhone\tEmail");
			while (resultSet.next()) {
				System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2)+"\t" + resultSet.getString(3)+"\t" + resultSet.getString(4)+"\t" + resultSet.getString(5));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (dbconnect != null) {
				dbconnect.close();
			}
			if (dbstatement != null) {
				dbstatement.close();
			}
		}
	}
	//delete account data from database
	public void deleteData(String id) throws SQLException {			
		Connection dbconnect = null;			
		PreparedStatement dbstatement = null;					
		try {				
			dbconnect = this.getConnection();				
			dbstatement = dbconnect.prepareStatement("DELETE FROM customer WHERE id=?");			
			dbstatement.setString(1, id);				
			dbstatement.executeUpdate();			
			} catch (ClassNotFoundException | SQLException e) {				
				e.printStackTrace();			
				} finally {				
					if (dbconnect != null ) {				
						dbconnect.close();			
						}			
					if (dbstatement != null ) {			
						dbstatement.close();		
						}		
					}		
		}
}	

	/*private String customId;
	private String address;
	private int phone;
	private String email;

	public Customer(String customId, String address, int phone, String email) {
		this.setCustomId(customId);
		this.setAddress(address);
		this.setPhone(phone);
		this.setEmail(email);

	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCustomId() {
		return customId;
	}

	public String getAddress() {
		return address;
	}

	public int getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public void printCustomer() {
		System.out.println(
				"Customer ID : " + customId + " Address: " + address + " Phone No : " + phone + " Email : " + email);

	}0*/


