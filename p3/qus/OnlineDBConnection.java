package Ap.p3.qus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OnlineDBConnection {
	public static void main(String[] args) {
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		System.out.println("Driver attached");
	} catch (ClassNotFoundException e) {
		System.err.println("driver not attached!");
		e.printStackTrace();
	}

	// connect database
	Connection dbconnect = null;

	try {
		dbconnect = DriverManager.getConnection("jdbc:mysql://localhost:3306/online_shopping", "root", "root");

	} catch (SQLException e) {

		e.printStackTrace();
	}

	// check db connection
	if (dbconnect != null) {
		System.out.println("database connected!");

	} else {
		System.out.println("database not connected!");
	}


}}
