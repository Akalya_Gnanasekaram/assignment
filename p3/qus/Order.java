package Ap.p3.qus;

enum OrderStatus {
	NEW, HOLDE, SHIPPED, DELIVERD, CLOSED;
}

public class Order extends Payment {
	private static final String NEW = "NEW ORDER";
	private static final String HOLDE = "HOLD ORDER";
	private static final String SHIPPED = "ORDER SHIPPED";
	private static final String DELIVERD = "ORDER DELIVERD";
	private static final String CLOSED = "ORDER CLOSED";
	private String id;
	private String orderdate;
	private String shippddate;
	private String shippedadd;
	private String total;

	public Order(String id, String orderdate, String shippddate, String shippedadd, String total) {
		this.id = id;
		this.setOrderdate(orderdate);
		this.setShippddate(shippddate);
		this.setShippedadd(shippedadd);
		this.setTotal(total);

	}

	public void statusprint() {
		switch (id) {
		case NEW: {
			System.out.println("New order!");
		}
			break;
		case HOLDE: {
			System.out.println("Holding");
		}
			break;
		case SHIPPED: {
			System.out.println("Order shipped");

		}
			break;
		case DELIVERD: {
			System.out.println("Order deliverd!");
		}
			break;
		case CLOSED: {
			System.out.println("Closed!");
		}
			break;
		default: {
			System.out.println("Invalid");
		}
		}

	}

	public String getOrderdate() {
		return orderdate;
	}

	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}

	public String getShippddate() {
		return shippddate;
	}

	public void setShippddate(String shippddate) {
		this.shippddate = shippddate;
	}

	public String getShippedadd() {
		return shippedadd;
	}

	public void setShippedadd(String shippedadd) {
		this.shippedadd = shippedadd;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
	
	
	
	public void printorder() {
		System.out.println("ID: "+ id+"\nOrder Date: "+ orderdate+"\nShipped Date: "+shippddate+"\nShipped Adderss: "+shippedadd+"\nTotal: "+total);
	}
}
