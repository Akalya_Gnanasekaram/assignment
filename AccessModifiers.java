
class Access {
	// private variable
	private int age;

	public int getAge() { //get method
		return age;
	}

	public void setAge(int age) { // set method
		this.age = age;
	}

}

public class AccessModifiers {

	public static void main(String[] main) {

		// create an object of Data
		Access a = new Access();

		// access private variable and field from another class
		a.setAge(23);
		System.out.println("Age : " + a.getAge());

	}
}